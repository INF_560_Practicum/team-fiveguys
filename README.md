# YueMa Application

The project is about utilizing data from Kiana.io and external data sources to build group-buying like application with the assistant of recommendation systems built from the data. The project includes project description, objectives, and the process of accomplishing the application, experiment results of the machine learning models, and the final web application with those machine learning malgorithms embedded. We also provided experiment results of the machine learning models and further suggestions for researchers in the future.

Application endpoint [click here](http://yuemahost-dev.s3-website-us-east-1.amazonaws.com)

## Project Architecture:
![readme image](./images/DPM.png "Readme Image")

## Contributors 

[Shangting Li](https://github.com/Shangtingli)

[Runze Liu](https://github.com/RenzoLiu0209)

[Yenchen Chou](https://github.com/yenchenchou)

[Yufei Hu](https://github.com/Leo-hyf)

Zhifeng Liu








